# php-mtg/php-keyrune-bridge

A library that brings the keyrune library to the php world.

![coverage](https://gitlab.com/php-mtg/php-keyrune-bridge/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-mtg/php-keyrune-bridge/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

You must add the asset-packagist to your `composer.json` :

```
	"repositories" : [{
			"type" : "composer",
			"url" : "https://asset-packagist.org",
			"name" : "asset-packagist"
		}
	]
```

The repository is mandatory as the npm packages do not appear in the generic
packagist repository. Then :

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Run the following command to install this library as dependency :
- `php composer.phar php-mtg/php-keyrune-bridge ^8`

/!\ WARNING /!\ This library may not deploy correctly under windows, due to the
presence of the `con.svg` file (representing the Conflux Set) present in the
tarball for the `npm-asset/keyrune` library downloaded from asset-packagist.

No solutions to this problem have been found so far.

The case on linux as subsystem for windows, for w10 users, has not been tested.


## Basic Usage

```php

use PhpMtg\Keyrune\Keyrune;

$rune = Keyrune::findBySetCode('10e'); // returns the Tenth Edition Rune
(string) $rune->getIconHtml(); // <i class="ss ss-10e"></i>

```

For the icon to appear on an html page, you have to publish the css and font
files (or uses a cdn like jsdeliver, as specified in the original readme
of [the keyrune library](https://github.com/andrewgioia/Keyrune)).

The methods `Keyrune::getCssFilePath()` and `Keyrune::getFontFilePaths()`
are made for this task, as they point to the existing files in the keyrune library,
under your vendor directory (as created by composer).


## License

MIT (See [LICENSE](LICENSE)) (for the php code only).

Note that this library uses the keyrune fonts and icons made by [Andrew Gioia](https://github.com/andrewgioia/Keyrune).

The css and svg files are under [GPL3.0](https://opensource.org/licenses/GPL-3.0).

The font files are under [SIL OFL 1.1](https://opensource.org/licenses/OFL-1.1).
