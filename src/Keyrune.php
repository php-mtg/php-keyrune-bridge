<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/php-keyrune-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpMtg\Keyrune\Keyrune;

use PhpExtended\Html\HtmlAbstractNodeInterface;
use PhpExtended\Html\HtmlAttribute;
use PhpExtended\Html\HtmlSingleNode;
use ReflectionException;

/**
 * Keyrune class file.
 * 
 * This class represents an extension symbol for a given set.
 * 
 * @author Anastaszor
 */
enum Keyrune : string
{
	
	/**
	 * Gets the absolute path on your system for the css file given by the
	 * keyrune library.
	 * 
	 * @param boolean $dev (if true, use the non-minified version)
	 * @return string
	 */
	public static function getCssFilePath(bool $dev = false) : string
	{
		$cssdir = \dirname(__DIR__, 2).\DIRECTORY_SEPARATOR.'npm-asset'.\DIRECTORY_SEPARATOR.'keyrune'.\DIRECTORY_SEPARATOR.'css';
		
		return $cssdir.\DIRECTORY_SEPARATOR.'keyrune'.((!$dev) ? '.min' : '').'.css';
	}
	
	/**
	 * Gets the absolute paths on your systems for the font files given by
	 * the keyrune library.
	 * 
	 * @param string $ext (if found, returns that specific file only)
	 * @return array<integer, string>
	 */
	public static function getFontFilePaths(?string $ext = null) : array
	{
		$fontdir = \dirname(__DIR__, 2).\DIRECTORY_SEPARATOR.'npm-asset'.\DIRECTORY_SEPARATOR.'keyrune'.\DIRECTORY_SEPARATOR.'fonts';
		
		switch($ext)
		{
			case 'eot':
			case 'svg':
			case 'ttf':
			case 'woff':
			case 'woff2':
				return [$fontdir.\DIRECTORY_SEPARATOR.'keyrune.'.$ext];
		}
		
		return [
			$fontdir.\DIRECTORY_SEPARATOR.'keyrune.eot',
			$fontdir.\DIRECTORY_SEPARATOR.'keyrune.svg',
			$fontdir.\DIRECTORY_SEPARATOR.'keyrune.ttf',
			$fontdir.\DIRECTORY_SEPARATOR.'keyrune.woff',
			$fontdir.\DIRECTORY_SEPARATOR.'keyrune.woff2',
		];
	}
	
	/**
	 * Gets the given keyrune if the set code is found in the library, or the
	 * default one otherwise.
	 * 
	 * @param string $setCode
	 * @param Keyrune $default
	 * @return null|Keyrune
	 * @throws ReflectionException
	 */
	public static function findBySetCode(string $setCode, ?Keyrune $default = null)
	{
		$setCode = (string) \mb_strtolower($setCode);
		
		foreach(static::cases() as $rune)
		{
			if($rune->value === $setCode)
			{
				return $rune;
			}
		}
		
		return $default;
	}
	
	/**
	 * Gets the icon html node. This can be easily transformed into an html
	 * string with the __toString() method.
	 * 
	 * @return HtmlAbstractNodeInterface
	 */
	public function getIconHtml() : HtmlAbstractNodeInterface
	{
		return new HtmlSingleNode('i', [new HtmlAttribute('class', 'ss ss-'.$this->value)]);
	}
	
	/**
	 * Gets the absolute path on your system of the svg file corresponding to
	 * that icon given by the keyrune library.
	 * 
	 * @return string
	 */
	public function getSvgFilePath() : string
	{
		$svgdir = \dirname(__DIR__, 2).\DIRECTORY_SEPARATOR.'npm-asset'.\DIRECTORY_SEPARATOR.'keyrune'.\DIRECTORY_SEPARATOR.'svg';
		
		return $svgdir.\DIRECTORY_SEPARATOR.$this->value.'.svg';
	}

	case _10E = '10e';

	case _1E = '1e';

	case _2E = '2e';

	case _2ED = '2ed';

	case _2U = '2u';

	case _3E = '3e';

	case _3ED = '3ed';

	case _4E = '4e';

	case _5DN = '5dn';

	case _5ED = '5ed';

	case _6ED = '6ed';

	case _7ED = '7ed';

	case _8ED = '8ed';

	case _9ED = '9ed';

	case A25 = 'a25';

	case AER = 'aer';

	case AKH = 'akh';

	case ALA = 'ala';

	case ALL = 'all';

	case ANN = 'ann';

	case APC = 'apc';

	case ARB = 'arb';

	case ARC = 'arc';

	case ARN = 'arn';

	case ATH = 'ath';

	case ATQ = 'atq';

	case AVR = 'avr';

	case BBD = 'bbd';

	case BFZ = 'bfz';

	case BNG = 'bng';

	case BOK = 'bok';

	case BRB = 'brb';

	case BTD = 'btd';

	case C13 = 'c13';

	case C14 = 'c14';

	case C15 = 'c15';

	case C16 = 'c16';

	case C17 = 'c17';

	case C18 = 'c18';

	case C19 = 'c19';

	case C20 = 'c20';

	case CC1 = 'cc1';

	case CHK = 'chk';

	case CHR = 'chr';

	case CM1 = 'cm1';

	case CM2 = 'cm2';

	case CMA = 'cma';

	case CMC = 'cmc';

	case CMD = 'cmd';

	case CMR = 'cmr';

	case CNS = 'cns';

	case CN2 = 'cn2';

	case CON = 'con';

	case CSP = 'csp';

	case DD2 = 'dd2';

	case DOC = 'doc';

	case DDD = 'ddd';

	case DDE = 'dde';

	case DDF = 'ddf';

	case DDG = 'ddg';

	case DDH = 'ddh';

	case DDI = 'ddi';

	case DDJ = 'ddj';

	case DDK = 'ddk';

	case DDL = 'ddl';

	case DDM = 'ddm';

	case DDN = 'ddn';

	case DDO = 'ddo';

	case DDP = 'ddp';

	case DDQ = 'ddq';

	case DDR = 'ddr';

	case DDS = 'dds';

	case DDT = 'ddt';

	case DDU = 'ddu';

	case DGM = 'dgm';

	case DIS = 'dis';

	case DKA = 'dka';

	case DKM = 'dkm';

	case DOM = 'dom';

	case DPA = 'dpa';

	case DRB = 'drb';

	case DRK = 'drk';

	case DST = 'dst';

	case DTK = 'dtk';

	case E01 = 'e01';

	case E02 = 'e02';

	case ELD = 'eld';

	case EMA = 'ema';

	case EMN = 'emn';

	case EVE = 'eve';

	case EVG = 'evg';

	case EXO = 'exo';

	case EXP = 'exp';

	case FEM = 'fem';

	case FRF = 'frf';

	case FUT = 'fut';

	case GK1 = 'gk1';

	case GK2 = 'gk2';

	case GN2 = 'gn2';

	case GNT = 'gnt';

	case GPT = 'gpt';

	case GRN = 'grn';

	case GS1 = 'gs1';

	case GTC = 'gtc';

	case H09 = 'h09';

	case H17 = 'h17';

	case HA1 = 'ha1';

	case HML = 'hml';

	case HOP = 'hop';

	case HOU = 'hou';

	case ICE = 'ice';

	case ICE2 = 'ice2';

	case IKO = 'iko';

	case IMA = 'ima';

	case INV = 'inv';

	case ISD = 'isd';

	case J20 = 'j20';

	case JOU = 'jou';

	case JUD = 'jud';

	case KLD = 'kld';

	case KTK = 'ktk';

	case LEA = 'lea';

	case LEB = 'leb';

	case LEG = 'leg';

	case LGN = 'lgn';

	case LRW = 'lrw';

	case M10 = 'm10';

	case M11 = 'm11';

	case M12 = 'm12';

	case M13 = 'm13';

	case M14 = 'm14';

	case M15 = 'm15';

	case M19 = 'm19';

	case M20 = 'm20';

	case M21 = 'm21';

	case MBS = 'mbs';

	case MD1 = 'md1';

	case ME1 = 'me1';

	case ME2 = 'me2';

	case ME3 = 'me3';

	case ME4 = 'me4';

	case MED = 'med';

	case MH1 = 'mh1';

	case MIR = 'mir';

	case MM2 = 'mm2';

	case MM3 = 'mm3';

	case MMA = 'mma';

	case MMQ = 'mmq';

	case MOR = 'mor';

	case MP1 = 'mp1';

	case MP2 = 'mp2';

	case MPS = 'mps';

	case MRD = 'mrd';

	case NEM = 'nem';

	case NMS = 'nms';

	case NPH = 'nph';

	case ODY = 'ody';

	case OGW = 'ogw';

	case ONS = 'ons';

	case ORI = 'ori';

	case P02 = 'p02';

	case PC2 = 'pc2';

	case PCA = 'pca';

	case PCY = 'pcy';

	case PD2 = 'pd2';

	case PD3 = 'pd3';

	case PLC = 'plc';

	case PLS = 'pls';

	case PO2 = 'po2';

	case POR = 'por';

	case PTG = 'ptg';

	case PTK = 'ptk';

	case RAV = 'rav';

	case RIX = 'rix';

	case XREN = 'xren';

	case XRIN = 'xrin';

	case RNA = 'rna';

	case ROE = 'roe';

	case RTR = 'rtr';

	case S00 = 's00';

	case S99 = 's99';

	case SCG = 'sgc';

	case SHM = 'shm';

	case SOI = 'soi';

	case SOK = 'sok';

	case SOM = 'som';

	case SS1 = 'ss1';

	case SS2 = 'ss2';

	case STH = 'sth';

	case TD2 = 'td2';

	case THB = 'thb';

	case THS = 'ths';

	case TMP = 'tmp';

	case TOR = 'tor';

	case TPR = 'tpr';

	case TSP = 'tsp';

	case UDS = 'uds';

	case UGL = 'ugl';

	case ULG = 'ulg';

	case UMA = 'uma';

	case UND = 'und';

	case UNH = 'unh';

	case UST = 'ust';

	case USG = 'usg';

	case V09 = 'v09';

	case V0X = 'v0x';

	case V10 = 'v10';

	case V11 = 'v11';

	case V12 = 'v12';

	case V13 = 'v13';

	case V14 = 'v14';

	case V15 = 'v15';

	case V16 = 'v16';

	case V17 = 'v17';

	case VAN = 'van';

	case VIS = 'vis';

	case VMA = 'vma';

	case W16 = 'w16';

	case W17 = 'w17';

	case WAR = 'war';

	case WTH = 'wth';

	case WWK = 'wwk';

	case XLN = 'xlk';

	case ZEN = 'zen';

	case ZNC = 'znc';

	case XZNR = 'xznr';

	case AZORIUS = 'azorius';

	case BOROS = 'boros';

	case DIMIR = 'dimir';

	case GOLGARI = 'golgari';

	case GRUUL = 'gruul';

	case IZZET = 'izzet';

	case ORZHOV = 'orzhov';

	case RAKDOS = 'rakdos';

	case SELESNYA = 'selesnya';

	case SIMIC = 'simic';

	case BCORE = 'bcore';

	case HTR = 'htr';

	case HTR17 = 'htr17';

	case PAPAC = 'papac';

	case PARL = 'parl';

	case PARL2 = 'parl2';

	case PARL3 = 'parl3';

	case PAST = 'past';

	case PBOOK = 'pbook';

	case PDEP = 'pdep';

	case PDRC = 'pdrc';

	case PEURO = 'peuro';

	case PFNM = 'pfnm';

	case PGRU = 'pgru';

	case PHEART = 'pheart';

	case PIDW = 'pidw';

	case PLEAF = 'pleaf';

	case PMEI = 'pmei';

	case PMODO = 'pmodo';

	case PMPS = 'pmps';

	case PMPU = 'pmpu';

	case PMTG1 = 'pmtg1';

	case PMTG2 = 'pmtg2';

	case PXBOX = 'pxbox';

	case PSALVAT05 = 'psalvat05';

	case PSALVAT11 = 'psalval11';

	case PSEGA = 'psega';

	case PSUM = 'psum';

	case PTSA = 'ptsa';

	case PZ1 = 'pz1';

	case PZ2 = 'pz2';

	case X2PS = 'x2ps';

	case X4EA = 'x4ea';

	case XCLE = 'xcle';

	case XDUELS = 'xduels';

	case XICE = 'xice';

	case XMODS = 'xmods';
	
}
