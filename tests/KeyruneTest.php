<?php declare(strict_types=1);

/*
 * This file is part of the php-mtg/php-keyrune-bridge library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpMtg\Keyrune\Keyrune;
use PHPUnit\Framework\TestCase;

/**
 * KeyruneTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpMtg\Keyrune\Keyrune
 *
 * @internal
 *
 * @small
 */
class KeyruneTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var Keyrune
	 */
	protected Keyrune $_object;
	
	public function testToString() : void {}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void {}
	
}
